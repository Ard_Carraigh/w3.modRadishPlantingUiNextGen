@echo off
rem ---------------------------------------------------
rem --- load settings
rem ---------------------------------------------------
call _settings_.bat
rem ---------------------------------------------------

rem ---------------------------------------------------
echo.
echo -- STARTING GAME
echo.

cd /D "%DIR_W3%\bin\x64\"

witcher3.exe -debugscripts

cd /D "%DIR_EXECUTION_START%"
