// ----------------------------------------------------------------------------
state CreateRadishPlanterUiMod in CModBootstrap extends BootstrapCreateMod {
    // ------------------------------------------------------------------------
    final function modCreate(): CMod {
        return new CRadishPlanterUiMod in parent;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
exec function radishplanter_clear_saved() {
    GetModStorage().remove('RadishPlanterUi');
}
// ----------------------------------------------------------------------------
class CRadFpUiQuitPopupCallback extends IModUiConfirmPopupCallback {
    public var callback: CRadishPlanterUiMod;

    public function OnConfirmed(action: String) {
        switch (action) {
            case "quit": return callback.doQuit();
        }
    }
}
// ----------------------------------------------------------------------------
state RadFP_Active in CRadishPlanterUiMod {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var s: SRadishPlacement;
        var distance: float;

        parent.modUtil.deactivateHud();
        parent.modUtil.hidePlayer();
        parent.modUtil.freezeTime();

        // refresh camera position to player position
        parent.configManager.setLastCamPosition(RadFpUi_createCamSettingsFor(RadFpUiCam_Empty));

        if (!parent.radFoliagePlanter.activate()) {
            // something went wrong
            parent.GotoState('RadFP_Sleeping', false, true);
        }
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theInput.SetContext('Exploration');
        parent.radFoliagePlanter.deactivate();

        parent.modUtil.unfreezeTime();
        parent.modUtil.restorePlayer();
        parent.modUtil.reactivateHud();
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'RADFP_Minimize') {
                parent.GotoState('RadFP_Sleeping', false, true);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFP_Sleeping in CRadishPlanterUiMod {
    // ------------------------------------------------------------------------
    event OnMaximize(action: SInputAction) {
        var entity : CEntity;
        var template : CEntityTemplate;

        if (IsPressed(action)) {
            if (!parent.radFoliagePlanter) {
                template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishplanterui\radplanterui.w3mod", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

                parent.radFoliagePlanter = (CRadishFoliagePlanterUi)entity;
                parent.radFoliagePlanter.init(parent.log, parent.configManager, parent.quitConfirmCallback);

                template = (CEntityTemplate)LoadResource("dlc\modtemplates\radishseeds\radish_modutils.w2ent", true);
                entity = theGame.CreateEntity(template,
                    thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());
                parent.modUtil = (CRadishModUtils)entity;
            }

            parent.PushState('RadFP_Active');
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CRadishPlanterUiMod extends CMod {
    default modName = 'RadishPlanterUi';
    default modAuthor = "rmemr";
    default modUrl = "http://www.nexusmods.com/witcher3/mods/3620/";
    default modVersion = '0.1';

    default logLevel = MLOG_DEBUG;
    // ------------------------------------------------------------------------
    protected var radFoliagePlanter: CRadishFoliagePlanterUi;
    protected var modUtil: CRadishModUtils;
    // ------------------------------------------------------------------------
    // UI stuff
    protected var quitConfirmCallback: CRadFpUiQuitPopupCallback;
    // ------------------------------------------------------------------------
    private var modConfigId: CName; default modConfigId = 'RadishPlanterUiConfig';
    protected var configManager: CRadishPlanterConfigManager;
    // ------------------------------------------------------------------------
    private var gameTime: GameTime;
    // ------------------------------------------------------------------------
    public function init() {
        super.init();

        configManager = new CRadishPlanterConfigManager in this;
        configManager.init(this.log, (CRadishPlanterConfigData)GetModStorage().load(modConfigId));

        this.registerListeners();

        // prepare view callback wiring
        quitConfirmCallback = new CRadFpUiQuitPopupCallback in this;
        quitConfirmCallback.callback = this;

        // store time on activation to reset from any interactive time changes
        gameTime = theGame.GetGameTime();
        PushState('RadFP_Sleeping');
    }
    // ------------------------------------------------------------------------
    event OnMinimize(action: SInputAction) {}
    event OnMaximize(action: SInputAction) {}
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnMinimize', 'RADFP_Minimize');
        theInput.RegisterListener(this, 'OnMaximize', 'RADFP_Maximize');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RADFP_Minimize');
        theInput.UnregisterListener(this, 'RADFP_Maximize');
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        var null: CRadishFoliagePlanterUi;

        // reset every change made to time
        theGame.SetGameTime(gameTime, true);

        this.radFoliagePlanter.doQuit();
        this.radFoliagePlanter = null;
        PopState();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
