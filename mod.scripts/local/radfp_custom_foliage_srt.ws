// ----------------------------------------------------------------------------
// Radish Foliange Planter UI: additional foliage srt definitions
//
struct SRadFoliage_CustomSrtCsvDef {
    // required path for foliage-repo csv
    var foliageRepo: String;
    // required path for foliage-meta csv
    var foliageMeta: String;
}
// ----------------------------------------------------------------------------
// add additional srt definitions provided as csv
// ----------------------------------------------------------------------------
function RADFP_getAdditionalSrtDefCsvs() : array<SRadFoliage_CustomSrtCsvDef> {
    var foliageCsvList: array<SRadFoliage_CustomSrtCsvDef>;

    // Note: see below for format of csvs
    foliageCsvList.PushBack(SRadFoliage_CustomSrtCsvDef(
        "dlc\dlccustomfoliage\data\custom-foliage-repo.csv",
        "dlc\dlccustomfoliage\data\custom-foliage-meta.csv"
    ));

    return foliageCsvList;
}
// ----------------------------------------------------------------------------
// Format of csvs
// ----------------------------------------------------------------------------
// custom-foliage-repo.csv:
// ----------------------------------------------------------------------------
// #;CAT1;CAT2;CAT3;ID;Caption
// ;bob-foliage;;;cufl_amanita;amanita
// ...
// ----------------------------------------------------------------------------
// custom-foliage-meta.csv:
// ----------------------------------------------------------------------------
// #;ID;size1;size2;size3;min.X;min.Y;min.Z;mip0;mip1;mip2;mip3;mip4;mip5;mip6;mip7;mip8;mip9
// ;cufl_amanita;0.400458;0.400271;0.57683605;-0.19072701;-0.216211;-0.037686;50;80;100;200;;;;;;
// ...
// ----------------------------------------------------------------------------
