// ----------------------------------------------------------------------------
abstract class CRadishFpUiFilteredList extends CModUiFilteredList {
    // ------------------------------------------------------------------------
    public function preselect(optional openCategories: bool) {
        if (items.Size() > 0) {
            selectedId = items[0].id;
            if (openCategories) {
                selectedCat1 = items[0].cat1;
                selectedCat2 = items[0].cat2;
                selectedCat3 = items[0].cat3;
            }
        }
    }
    // ------------------------------------------------------------------------
    public function getSelectedId() : String {
        return selectedId;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
