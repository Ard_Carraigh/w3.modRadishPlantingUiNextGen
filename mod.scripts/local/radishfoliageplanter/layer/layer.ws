// ----------------------------------------------------------------------------
struct SRadFoliageLayerId {
    var layerName: String;
    var readonly: bool;
    var encoded: bool;
}
// ----------------------------------------------------------------------------
class CRadishFoliageSet {
    var type: array<int>;         // as in FoliageListProvider
    var pos: array<Vector>;       // faster access than from entity
    var rot: array<float>;
    var scale: array<float>;
    var proxies: array<CRadishFoliageEntity>;
    // original slot in layer so updates in params (e.g. pos, rot, scale)
    // can be propagated back to layer
    var slotInLayer: array<int>;
}
// ----------------------------------------------------------------------------
// three types of layers:
//  encoded + read-only:    static info about layercaption, not cloneable
//  encoded + editable:     contains foliage info, cloning layer makes it editable
//  editable:               creating new layer, adding/deleting foliage is possible
//
// ----------------------------------------------------------------------------
class CRadishFoliageLayer {
    // ------------------------------------------------------------------------
    protected var id: SRadFoliageLayerId;
    private var caption: String;
    protected var visibility: bool;
    // ------------------------------------------------------------------------
    // foliage data (arrays are synced as structure of arrays):
    //   slot i refers to i'th foliage entity
    // ------------------------------------------------------------------------
    // data stored as dbg infos?
    protected var type: array<int>;         // as in FoliageListProvider,
                                            // synced ordering with encoder repo
    protected var pos: array<Vector>;
    protected var rot: array<float>;        // only yaw supported in flyr files
    protected var scale: array<float>;      // only one scale value in flyr files

    protected var proxies: array<CRadishFoliageEntity>;
    protected var foliageList: CRadishFoliageList;
    // ------------------------------------------------------------------------
    public function init(id: SRadFoliageLayerId, worldId: String) {
        this.setId(id);
        //settings.world = worldId;

        refreshCaption();
    }
    // ------------------------------------------------------------------------
    public function setFoliageList(foliageList: CRadishFoliageList) {
        this.foliageList = foliageList;
    }
    // ------------------------------------------------------------------------
    public function isEditable() : bool {
        return !(this.id.encoded || this.id.readonly);
    }
    // ------------------------------------------------------------------------
    public function isDeleteable() : bool {
        return !(this.id.encoded || this.id.readonly);
    }
    // ------------------------------------------------------------------------
    public function isCloneable() : bool {
        return !this.id.readonly;
    }
    // ------------------------------------------------------------------------
    public function isRenameable() : bool {
        return !this.id.readonly;
    }
    // ------------------------------------------------------------------------
    public function cloneFrom(src: CRadishFoliageLayer) {
        var i, s: int;
        var entity: CRadishFoliageEntity;

        this.visibility = true;

        destroy();

        s = src.type.Size();
        for (i = 0; i < s; i += 1) {
           // new foliage -> spawn
            entity = new CRadishFoliageEntity in this;
            entity.init(
                foliageList.getStringId(src.type[i]),
                foliageList.getMetaInfo(src.type[i]),
                src.pos[i], src.rot[i], src.scale[i]);

            proxies.PushBack(entity);
            type.PushBack(src.type[i]);
            pos.PushBack(src.pos[i]);
            rot.PushBack(src.rot[i]);
            scale.PushBack(src.scale[i]);
        }

        refreshCaption();
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        var i, s: int;

        s = proxies.Size();
        for (i = 0; i < s; i += 1) {
            proxies[i].destroy();
        }
        proxies.Clear();
        type.Clear();
        pos.Clear();
        rot.Clear();
        scale.Clear();
    }
    // ------------------------------------------------------------------------
    // foliage data
    // ------------------------------------------------------------------------
    public function getInRange(
        center: Vector, radius: float, optional filter: array<int>) : CRadishFoliageSet
    {
        var i, s: int;
        var foliage: CRadishFoliageSet;
        var distance: float;
        var doFilter: bool;

        foliage = new CRadishFoliageSet in this;

        distance = radius * radius;
        center.Z = 0;
        center.W = 0;

        doFilter = filter.Size() > 0;

        s = type.Size();
        for (i = 0; i < s; i += 1) {
            if ((doFilter && !filter.Contains(type[i]))
                || VecDistanceSquared2D(Vector(pos[i].X, pos[i].Y), center) > distance)
            {
                continue;
            }
            foliage.type.PushBack(type[i]);
            foliage.pos.PushBack(pos[i]);
            foliage.rot.PushBack(rot[i]);
            foliage.scale.PushBack(scale[i]);
            foliage.proxies.PushBack(proxies[i]);
            foliage.slotInLayer.PushBack(i);
        }

        return foliage;
    }
    // ------------------------------------------------------------------------
    public function updateSet(updated: CRadishFoliageSet) : bool {
        var i, s: int;
        var updatedType: int;
        var updatedSlot: int;
        var entity: CRadishFoliageEntity;
        var cleanupDespawned: bool;

        s = updated.type.Size();

        for (i = 0; i < s; i += 1) {
            // update parameters, spawn newly added, remove deleted ones
            updatedType = updated.type[i];
            updatedSlot = updated.slotInLayer[i];

            if (updatedType < 0 && updatedSlot >= 0) {
                // despawn
                type[updatedSlot] = -1;
                proxies[updatedSlot].destroy();
                cleanupDespawned = true;
            }

            if (updatedType >= 0) {
                if (updatedSlot < 0) {
                    // new foliage -> spawn
                    entity = new CRadishFoliageEntity in this;
                    entity.init(
                        foliageList.getStringId(updatedType),
                        foliageList.getMetaInfo(updatedType),
                        updated.pos[i], updated.rot[i], updated.scale[i]);

                    proxies.PushBack(entity);
                    type.PushBack(updatedType);
                    pos.PushBack(updated.pos[i]);
                    rot.PushBack(updated.rot[i]);
                    scale.PushBack(updated.scale[i]);
                } else {
                    // updated foliage -> change params
                    // type does not change
                    pos[updatedSlot] = updated.pos[i];
                    rot[updatedSlot] = updated.rot[i];
                    scale[updatedSlot] = updated.scale[i];
                    proxies[updatedSlot].update(
                        updated.pos[i], updated.rot[i], updated.scale[i]);
                }
            }
        }
        if (cleanupDespawned) {
            cleanupData();
        }
        return false;
    }
    // ------------------------------------------------------------------------
    protected function cleanupData() {
        var i, s: int;
        var new_type: array<int>;
        var new_pos: array<Vector>;
        var new_rot: array<float>;
        var new_scale: array<float>;
        var new_proxies: array<CRadishFoliageEntity>;

        s = proxies.Size();
        for (i = 0; i < s; i += 1) {
            if (type[i] >= 0) {
                new_type.PushBack(type[i]);
                new_pos.PushBack(pos[i]);
                new_rot.PushBack(rot[i]);
                new_scale.PushBack(scale[i]);
                new_proxies.PushBack(proxies[i]);
            }
        }

        this.type = new_type;
        this.pos = new_pos;
        this.rot = new_rot;
        this.scale = new_scale;
        this.proxies = new_proxies;
    }
    // ------------------------------------------------------------------------
    public function getId() : SRadFoliageLayerId {
        return this.id;
    }
    // ------------------------------------------------------------------------
    public function getIdString() : String {
        return this.id.layerName;
    }
    // ------------------------------------------------------------------------
    // naming
    // ------------------------------------------------------------------------
    public function getName() : String {
        return this.id.layerName;
    }
    // ------------------------------------------------------------------------
    public function getCaption() : String {
        return this.caption;
    }
    // ------------------------------------------------------------------------
    public function getExtendedCaption() : String {
        var prefix: String;

        if (visibility) { prefix = "v "; } else { prefix = ". "; }

        return prefix + this.caption + " [" + IntToString(getItemCount()) + "]";
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    public function getItemCount() : int {
        return type.Size();
    }
    // ------------------------------------------------------------------------
    private function refreshCaption() {
        caption = StrReplaceAll(id.layerName, "_", " ");
    }
    // ------------------------------------------------------------------------
    public function toggleVisibility() {
        show(!visibility);
    }
    // ------------------------------------------------------------------------
    public function show(doShow: bool) {
        var i, s: int;

        if (visibility != doShow) {
            visibility = doShow;
            s = proxies.Size();
            for (i = 0; i < s; i += 1) {
                proxies[i].show(doShow);
            }
        }
    }
    // ------------------------------------------------------------------------
    // setter
    // ------------------------------------------------------------------------
    public function setId(newId: SRadFoliageLayerId) {
        id = newId;
        id.layerName = RadUi_escapeAsId(newId.layerName);
        refreshCaption();
    }
    // ------------------------------------------------------------------------
    public function setName(newName: String) {
        id.layerName = RadUi_escapeAsId(newName);
        refreshCaption();
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function asDefinition() : SEncValue {
        var content, foliageInstance: SEncValue;
        var i, s, partitionSlot: int;
        var typePartition: array<SEncValue>;
        var uniqueTypeIds: array<int>;
        var foliageType: String;

        content = SEncValue(EEVT_Map);

        if (getItemCount() > 0) {
            s = type.Size();

            // prepare partitions per type
            for (i = 0; i < s; i += 1) {
                if (!uniqueTypeIds.Contains(type[i])) {
                    uniqueTypeIds.PushBack(type[i]);
                    typePartition.PushBack(encValueNewList());
                }
            }

            // fill partitions
            // - [ 19.9725933075, -139.0117340088, 1.3718924522, 0.9, 10.0 ]
            for (i = 0; i < s; i += 1) {
                partitionSlot = uniqueTypeIds.FindFirst(type[i]);

                foliageInstance = encValueNewList();
                encListPush(FloatToEncValue(pos[i].X), foliageInstance);
                encListPush(FloatToEncValue(pos[i].Y), foliageInstance);
                encListPush(FloatToEncValue(pos[i].Z), foliageInstance);
                encListPush(FloatToEncValue(scale[i]), foliageInstance);
                encListPush(FloatToEncValue(rot[i]), foliageInstance);

                encListPush(foliageInstance, typePartition[partitionSlot]);
            }

            // editable: true -> encoder generates editable instance and not
            // final flyr files
            encMapPush_bool_opt(".editable", false, false, content);

            // attach to layer root definition
            for (i = 0; i < uniqueTypeIds.Size(); i += 1) {
                foliageType = foliageList.getStringId(uniqueTypeIds[i]);

                encMapPush(foliageType, typePartition[i], content);
                content.m.PushBack(SeperatorToEncKeyValue());
            }
        }

        return content;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
