// ----------------------------------------------------------------------------
class CRadPlanterModeView {
    // ------------------------------------------------------------------------
    private var view: CRadishPlanterUiPopupCallback;
    private var editor: CRadishFoliageEditor;
    // ------------------------------------------------------------------------
    private var layerCaption: String;
    private var opInProgress: String;
    private var opInProgressColor: String;
    // ------------------------------------------------------------------------
    public function init(modeView: CRadishPlanterUiPopupCallback, editor: CRadishFoliageEditor) {
        this.view = modeView;
        this.editor = editor;
    }
    // ------------------------------------------------------------------------
    public function setLayerCaption(caption: String) {
        layerCaption = GetLocStringByKeyExt("RADFP_lLayerCaption") + " " + caption;
    }
    // ------------------------------------------------------------------------
    public function refresh() {
        var info: String;

        view.menuRef.setTextField1(layerCaption);
        view.menuRef.setTextField2(GetLocStringByKeyExt(editor.currentMode()));
        view.menuRef.setTextField3(
            "[" + GetLocStringByKeyExt("RADFP_lBrushSize") + " "
            + editor.brushSizeInfo() + "] "
            + GetLocStringByKeyExt("RADFP_lBrushCaption") + " "
            + editor.brushName());

        if (opInProgress != "") {
            view.menuRef.setTextField4(opInProgress, opInProgressColor);
        } else {
            editor.OnCurrentParamInfo(info);
            view.menuRef.setTextField4(info);
        }
    }
    // ------------------------------------------------------------------------
    public function setOperationInProgress(opString: String, optional color: String) {
        opInProgress = opString;
        opInProgressColor = color;
        refresh();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFp_FoliageEditing in CRadishFoliagePlanterUi extends Rad_InteractivePlacement {
    // ------------------------------------------------------------------------
    default workContext = 'MOD_RadFp_FoliageEditing';
    // ------------------------------------------------------------------------
    private var menuConf: SModUiTopMenuConfig;
    private var modeView: CRadPlanterModeView;
    // ------------------------------------------------------------------------
    private var brushlistProvider: CRadishFoliageBrushList;
    private var foliageEditor: CRadishFoliageEditor;
    private var selectedLayer: CRadishFoliageLayer;
    // ------------------------------------------------------------------------
    default snapToGround = true;
    default isGroundSnapable = false;

    default isMoveable = true;
    default isUpDownMoveable = false;
    default isRotatableYaw = false;
    default isRotatablePitch = false;
    default isRotatableRoll = false;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var brushPlacement, camPlacement: SRadishPlacement;

        parent.currentModeId = "FoliagePlanting";
        parent.workContext = this.workContext;

        brushlistProvider = parent.brushManager.getBrushList();

        selectedLayer = parent.layerManager.getSelected();
        foliageEditor = parent.foliageEditor;

        camPlacement = parent.theCam.getSettings();
        parent.terrainProbe.precacheFor(camPlacement.pos, camPlacement.rot.Yaw);

        brushPlacement.pos = RadFpUi_getGroundPosFromCam(camPlacement);
        foliageEditor.getBrush().setPlacement(brushPlacement);
        foliageEditor.getBrush().setCamPosition(camPlacement.pos);
        foliageEditor.setBrushParams(brushlistProvider.getSelectedBrushParams());

        menuConf = SModUiTopMenuConfig(
            40.0, 15.0, 60.0, 25.0, 55.0,        // x, y, width, height, alpha
            SModUiTextFieldConfig(              // title field:
                1, 17, 130, 25, 17                  // x, y, width, height, fontsize
            ),
            SModUiTextFieldConfig(              // title field:
                3, 62, 230, 25, 13                  // x, y, width, height, fontsize
            ),
            SModUiTextFieldConfig(              // title field:
                3, 105, 330, 25, 12                  // x, y, width, height, fontsize
            ),
            SModUiTextFieldConfig(              // title field:
                3, 144, 330, 25, 10                  // x, y, width, height, fontsize
            ),
        );

        parent.showUi(false);
        theGame.RequestPopup('Test2Popup', parent.viewPopup);
        if (parent.viewPopup.menuRef) {
            parent.viewPopup.menuRef.setMenuConfig(menuConf);
            parent.viewPopup.menuRef.setupFields();
        }
        modeView = new CRadPlanterModeView in this;
        modeView.init(parent.viewPopup, foliageEditor);
        modeView.setLayerCaption(selectedLayer.getCaption());
        modeView.refresh();

        // modeView enables to trigger refreshes (e.g. opsInProgress, lockedBrushes)
        foliageEditor.activate(selectedLayer, modeView);

        // selectedElement is the moving proxy for placement mode state (== brush visualizer entity)
        selectedElement = foliageEditor.getBrushVisualizer();

        super.OnEnterState(prevStateName);

        // use distance to cam to increase placement speed
        ((CRadishInteractiveFoliagePlacement)theController).setCamPosition(camPlacement.pos);

        if (parent.terrainProbe.needsCaching()) {
            precacheTerrainHeight();
        }
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theGame.ClosePopup('Test2Popup');

        super.OnLeaveState(nextStateName);

        foliageEditor.deactivate();
    }
    // ------------------------------------------------------------------------
    private entry function precacheTerrainHeight() {
        var camPlacement: SRadishPlacement;
        var nextPos: Vector;

        camPlacement = parent.theCam.getSettings();
        foliageEditor.disableCommands();
        modeView.setOperationInProgress(
            GetLocStringByKeyExt("RADFP_iCachingTerrainHeight"), "#ff3333");

        while (parent.terrainProbe.needsCaching()) {
            if (parent.terrainProbe.getNextToBeCachedPos(nextPos)) {
                parent.positionTracker.updateTrackedPos(SRadishPlacement(nextPos));
                SleepOneFrame();
            }
        }

        parent.positionTracker.updateTrackedPos(camPlacement);
        foliageEditor.enableCommands();
        modeView.setOperationInProgress("");
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        parent.backToPreviousState(action);
    }
    // ------------------------------------------------------------------------
    protected function createInteractivePlacement() : CRadishInteractivePlacement {
        var camPos: SRadishPlacement;
        var placement: CRadishInteractiveFoliagePlacement;
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource(
            "dlc\modtemplates\radishplanterui\base\interactive_placement.w2ent", true);
        placement = (CRadishInteractiveFoliagePlacement)theGame.CreateEntity(
            template, thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        camPos = parent.theCam.getSettings();

        placement.setConfig(parent.getConfig());
        placement.setCameraHeading(camPos.rot.Yaw);
        placement.setTracker((CRadishTerrainTracker)parent.theCam.getTracker());

        return placement;
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {}
    // ------------------------------------------------------------------------
    event OnUpdatePopupView() {
        // called on opened (if menu was closed)
        parent.viewPopup.menuRef.setMenuConfig(menuConf);
        parent.viewPopup.menuRef.setupFields();
        modeView.refresh();
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        foliageEditor.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('RADFP_PlantingMode'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_PlacementMode'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ScalingMode'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_DeletingMode'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SelectBrush'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SelectPrevBrush'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SelectNextBrush'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleSlow'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_AdjustTime'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ToggleInteractiveCam', 'RAD_ToggleInteractiveCam', IK_LControl));

        parent.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
    event OnInteractiveTime(action: SInputAction) {
        if (!foliageEditor.opInProgress() && IsReleased(action)) {
            parent.PushState('RadFp_InteractiveTime');
        }
    }
    // ------------------------------------------------------------------------
    event OnInteractiveCam(action: SInputAction) {
        if (!foliageEditor.opInProgress() && IsReleased(action)) {
            parent.showUi(false);
            parent.PushState('RadFp_InteractiveCamera');
        }
    }
    // ------------------------------------------------------------------------
    event OnSelectBrushFromList(action: SInputAction) {
        if (!foliageEditor.opInProgress() && IsReleased(action)) {
            parent.PushState('RadFp_InteractiveBrushSelection');
        }
    }
    // ------------------------------------------------------------------------
    event OnQuickSelectBrush(action: SInputAction) {
        var quickAccesDef: array<SRadFoliage_BrushQuickAccess>;
        var prevBrushId, brushId: String;
        var params: SRadFoliageBrushDef;
        var i: int;

        if (!foliageEditor.opInProgress() && IsReleased(action)) {
            quickAccesDef = RADFP_getQuickaccesBrushIds();

            for (i = 0; i < quickAccesDef.Size(); i += 1) {
                if (quickAccesDef[i].slot == NameToString(action.aName)) {
                    brushId = quickAccesDef[i].brushId;

                    prevBrushId = brushlistProvider.getSelectedId();

                    if (brushlistProvider.setSelection(brushId, true)) {
                        params = brushlistProvider.getSelectedBrushParams();

                        if (params.brushName != "") {
                            foliageEditor.setBrushParams(params);
                            return true;
                        }
                    }

                    // restore previous brush
                    brushlistProvider.setSelection(prevBrushId, true);
                    parent.error(GetLocStringByKeyExt("RADFP_eQuickAccessSlotInvalidBrushId")
                        + " " + brushId);
                    return true;
                }
            }
            parent.error(GetLocStringByKeyExt("RADFP_eQuickAccessSlotNotAssigned"));
        }
    }
    // ------------------------------------------------------------------------
    event OnCycleBrush(action: SInputAction) {
        var optionId: String;

        if (!foliageEditor.opInProgress() && IsReleased(action)) {
            if (action.aName == 'RADFP_SelectPrevBrush') {
                optionId = brushlistProvider.getPreviousId();
            } else {
                optionId = brushlistProvider.getNextId();
            }
            OnSelectedBrush(optionId);
        }
    }
    // ------------------------------------------------------------------------
    event OnSelectedBrush(brushId: String) {
        if (brushlistProvider.setSelection(brushId, true)) {
            foliageEditor.setBrushParams(brushlistProvider.getSelectedBrushParams());
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeEditorMode(action: SInputAction) {
        var overlayPopupRef  : CR4OverlayPopup;

        if (!foliageEditor.opInProgress() && IsReleased(action)) {
            foliageEditor.changeMode(action.aName);
            // if placement mode is activated switch to a specialized moveable
            // element (with preview for foliage placement)
            // otherwise update to default brush
            selectedElement = foliageEditor.getBrushVisualizer();
            ((CRadishInteractiveFoliagePlacement)theController).setAsset(selectedElement);
            modeView.refresh();
        }
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        if (!foliageEditor.opInProgress()) {
            // immediately stop interactive movement to prevent weird behaviour with
            // view opening of previous state (no idea what the root cause is...)
            theController.stopInteractiveMode();
            backToPreviousState(action);
        } else {
            foliageEditor.cancelOp();
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeSpeed(action: SInputAction) {
        // adjusts intensity of brush
        if (IsPressed(action)) {
            if (action.aName == 'RAD_ToggleFast') {
                foliageEditor.changeStepSize("fast");
            } else {
                foliageEditor.changeStepSize("slow");
            }
        } else if (IsReleased(action)) {
            foliageEditor.changeStepSize("normal");
        }
        // and changes brush movement speed, too
        super.OnChangeSpeed(action);

        modeView.refresh();
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        parent.registerListeners();

        theInput.RegisterListener(this, 'OnBack', 'RAD_Back');
        theInput.RegisterListener(this, 'OnChangeEditorMode', 'RADFP_PlantingMode');
        theInput.RegisterListener(this, 'OnChangeEditorMode', 'RADFP_PlacementMode');
        theInput.RegisterListener(this, 'OnChangeEditorMode', 'RADFP_ScalingMode');
        theInput.RegisterListener(this, 'OnChangeEditorMode', 'RADFP_DeletingMode');

        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleFast');
        theInput.RegisterListener(this, 'OnChangeSpeed', 'RAD_ToggleSlow');

        theInput.RegisterListener(this, 'OnInteractiveTime', 'RADFP_AdjustTime');
        theInput.RegisterListener(this, 'OnInteractiveCam', 'RADFP_ToggleInteractiveCam');
        theInput.RegisterListener(this, 'OnSelectBrushFromList', 'RADFP_SelectBrush');
        theInput.RegisterListener(this, 'OnCycleBrush', 'RADFP_SelectPrevBrush');
        theInput.RegisterListener(this, 'OnCycleBrush', 'RADFP_SelectNextBrush');
        theInput.RegisterListener(this, 'OnQuickSelectBrush', 'RADFP_QuickSelectBrush1');
        theInput.RegisterListener(this, 'OnQuickSelectBrush', 'RADFP_QuickSelectBrush2');
        theInput.RegisterListener(this, 'OnQuickSelectBrush', 'RADFP_QuickSelectBrush3');
        theInput.RegisterListener(this, 'OnQuickSelectBrush', 'RADFP_QuickSelectBrush4');
        theInput.RegisterListener(this, 'OnQuickSelectBrush', 'RADFP_QuickSelectBrush5');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        parent.unregisterListeners();

        theInput.UnregisterListener(this, 'RAD_Back');
        theInput.UnregisterListener(this, 'RADFP_PlantingMode');
        theInput.UnregisterListener(this, 'RADFP_PlacementMode');
        theInput.UnregisterListener(this, 'RADFP_ScalingMode');
        theInput.UnregisterListener(this, 'RADFP_DeletingMode');

        theInput.UnregisterListener(this, 'RAD_ToggleFast');
        theInput.UnregisterListener(this, 'RAD_ToggleSlow');

        theInput.UnregisterListener(this, 'RADFP_AdjustTime');
        theInput.UnregisterListener(this, 'RADFP_ToggleInteractiveCam');
        theInput.UnregisterListener(this, 'RADFP_SelectBrush');
        theInput.UnregisterListener(this, 'RADFP_SelectPrevBrush');
        theInput.UnregisterListener(this, 'RADFP_SelectNextBrush');
        theInput.UnregisterListener(this, 'RADFP_QuickSelectBrush1');
        theInput.UnregisterListener(this, 'RADFP_QuickSelectBrush2');
        theInput.UnregisterListener(this, 'RADFP_QuickSelectBrush3');
        theInput.UnregisterListener(this, 'RADFP_QuickSelectBrush4');
        theInput.UnregisterListener(this, 'RADFP_QuickSelectBrush5');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
