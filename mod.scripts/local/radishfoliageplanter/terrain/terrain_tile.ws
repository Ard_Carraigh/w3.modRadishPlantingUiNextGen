// ----------------------------------------------------------------------------
class CRadTerrainHeightTile {
    // ------------------------------------------------------------------------
    private var heights: array<float>;
    private var cornerPos: Vector;
    private var queue: array<Vector>;
    private var failedPoints: int;
    // ------------------------------------------------------------------------
    public function init(startX: int, startY: int) {
        var i, x, y: int;

        failedPoints = 0;
        cornerPos = Vector(startX, startY);
        // init with invalid heights
        for (i = 0; i < 400; i += 1) {
            heights.PushBack(0.0);
        }

        for (y = 0; y < 20; y += 2) {
            for (x = 0; x < 20; x += 1) {
                queue.PushBack(cornerPos + Vector(x * 5.0, y * 5.0));
            }
            for (x = 19; x >= 0; x -= 1) {
                queue.PushBack(cornerPos + Vector(x * 5.0, (y + 1) * 5.0));
            }
        }
    }
    // ------------------------------------------------------------------------
    public function isDone() : bool {
        return queue.Size() == 0;
    }
    // ------------------------------------------------------------------------
    public function getFailedCount() : int {
        return failedPoints;
    }
    // ------------------------------------------------------------------------
    private function vecToSlot(pos: Vector): int {
        var slot: int;
        var localPos: Vector;

        localPos = pos - cornerPos;
        slot = RoundF(localPos.Y / 5.0) * 20 + RoundF(localPos.X / 5.0);

        return slot;
    }
    // ------------------------------------------------------------------------
    public function getNextToBeCachedPos(out pos: Vector) : bool {
        var tmp, groundPos, groundNormal: Vector;
        var nextPos: Vector;
        var theWorld: CWorld;
        var slot: int;

        theWorld = theGame.GetWorld();

        while (queue.Size() > 0) {
            nextPos = queue.Last();

            nextPos.W = 1.0;
            nextPos.Z = 5000;
            tmp = nextPos;
            tmp.Z = -5000;

            slot = vecToSlot(nextPos);
            if (theWorld.StaticTrace(tmp, nextPos, groundPos, groundNormal)) {
                heights[slot] = groundPos.Z;
                queue.PopBack();
            } else {
                pos = nextPos;

                // it takes some time to load collision data but if the terrain tile
                // was created without collision data this will be an endless loop
                // -> try only a limited number of times if player is already near
                // the requested position (== collision data should be loaded at some point)
                tmp = thePlayer.GetWorldPosition();
                tmp.Z = 0;
                tmp.W = 1;
                nextPos.Z = 0;

                heights[slot] += 1;

                // if player is already near increase counter faster
                if (VecLength(tmp - nextPos) < 3.0) {
                    heights[slot] += 2;
                }

                // 7 is just a heuristic parameter that seems to work
                if (heights[slot] < 7) {
                    return true;
                } else {
                    // give up.. there seems to be no collision info available
                    heights[slot] = -10000.0;
                    failedPoints += 1;
                    queue.PopBack();
                }
            }
        }
        //this.visualize();
        LogChannel('DEBUG', "CRadTerrainHeightTile.failedPoints: " + IntToString(failedPoints));
        return false;
    }
    // ------------------------------------------------------------------------
    public function getGroundPos(pos: Vector, out groundPos: Vector) : bool {
        var localPos, gridPos, direction: Vector;
        var slot1, slot2, slot3, x, y: int;
        var h1, h2, h3, delta_h1h2, delta_h1h3: float;

        localPos = pos - cornerPos;

        x = RoundF(localPos.X / 5.0);
        y = RoundF(localPos.Y / 5.0);

        gridPos = Vector(x * 5.0, y * 5.0);

        if (localPos.X >= gridPos.X) {
            direction.X = 5.0;
        } else {
            direction.X = -5.0;
        }

        if (localPos.Y >= gridPos.Y) {
            direction.Y = 5.0;
        } else {
            direction.Y = -5.0;
        }

        slot1 = y * 20 + x;
        slot2 = vecToSlot(pos + Vector(direction.X, 0.0));
        slot3 = vecToSlot(pos + Vector(0.0, direction.Y));

        groundPos = pos;

        if (slot1 >= heights.Size()) {
            return false;
        }
        h1 = heights[slot1];
        h2 = heights[slot2];
        h3 = heights[slot3];

        if (h1 < -9999.0) {
            // at least the main height must be valid
            return false;
        }

        if (slot2 < heights.Size() && h2 > -9999) {
            delta_h1h2 = (h2 - h1) * (localPos.X - gridPos.X) / 5.0;
        }
        if (slot3 < heights.Size() && h3 > -9999) {
            delta_h1h3 = (h3 - h1) * (localPos.Y - gridPos.Y) / 5.0;
        }

        groundPos.Z = h1 + delta_h1h2 + delta_h1h3;
        return true;
    }
    // ------------------------------------------------------------------------
    public function visualize() {
        var template: CEntityTemplate;
        var x,y: int;
        var pos: Vector;
        var height: float;

        template = (CEntityTemplate)LoadResource(
            "engine\templates\editor\markers\review\opened_flag.w2ent", true);

        for (y = 0; y < 20; y += 2) {
            for (x = 0; x < 20; x += 1) {
                height = heights[y * 20 + x];
                pos = cornerPos + Vector(x * 5.0, y * 5.0, height);
                theGame.CreateEntity(template, pos);
            }
            for (x = 19; x >= 0; x -= 1) {
                height = heights[(y + 1) * 20 + x];
                pos = cornerPos + Vector(x * 5.0, (y + 1) * 5.0, height);
                theGame.CreateEntity(template, pos);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadTerrainHeightStripe {
    // ------------------------------------------------------------------------
    private var initialized: bool;
    // ------------------------------------------------------------------------
    private var offsetX: int;
    private var offsetY: int;
    private var tileCount: int;
    private var tiles: array<CRadTerrainHeightTile>;
    // ------------------------------------------------------------------------
    public function isInitialized() : bool {
        return initialized;
    }
    // ------------------------------------------------------------------------
    public function initParameters(tileCount: int, offsetX: int, offsetY: int) {
        this.tileCount = tileCount;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }
    // ------------------------------------------------------------------------
    public function initTiles() {
        var tile: CRadTerrainHeightTile;
        var i: int;

        if (!initialized) {
            for (i = 0; i < tileCount; i += 1) {
                tile = new CRadTerrainHeightTile in this;
                // tiles are always 100x100
                tile.init(offsetX + i * 100, offsetY);
                tiles.PushBack(tile);
            }
            initialized = true;
        }
    }
    // ------------------------------------------------------------------------
    public function tile(tileX: int) : CRadTerrainHeightTile {
        return tiles[tileX];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
