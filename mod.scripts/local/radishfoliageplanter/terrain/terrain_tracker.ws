// ----------------------------------------------------------------------------
class CRadishTerrainTracker extends CRadishTracker {
    // ------------------------------------------------------------------------
    protected function createTerrainProbe() {
        terrainProbe = new CRadishCachingTerrainProbe in this;
        ((CRadishCachingTerrainProbe)terrainProbe).init();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
