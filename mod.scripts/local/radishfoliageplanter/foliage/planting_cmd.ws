// ----------------------------------------------------------------------------
abstract class CRadishPlantingCmd {
    // ------------------------------------------------------------------------
    protected var brushCenter: Vector;
    protected var brushRadius: float;
    protected var brushParams: SRadFoliageBrushDef;
    protected var foliageSet: CRadishFoliageSet;
    // ------------------------------------------------------------------------
    public function init(
        brushCenter: Vector, brushRadius: float, brushParams: SRadFoliageBrushDef,
        foliageSet: CRadishFoliageSet)
    {
        this.brushCenter = brushCenter;
        this.brushRadius = brushRadius;
        this.brushParams = brushParams;
        this.foliageSet = foliageSet;
    }
    // ------------------------------------------------------------------------
    public function execute() : CRadishFoliageSet;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
abstract class CRadishDistanceAwarePlantCmd extends CRadishPlantingCmd {
    // ------------------------------------------------------------------------
    protected var ySlicedBuckets: array<array<int>>;
    protected var sliceHeight: float;
    protected var minY: Float;
    // ------------------------------------------------------------------------
    protected var densityDistance: Float;
    protected var probability: Float;
    // ------------------------------------------------------------------------
    public function setParams(probability: Float, densityDistance: Float) {
        this.densityDistance = densityDistance;
        this.probability = probability;
        prepareDistanceChecks();
    }
    // ------------------------------------------------------------------------
    private function addSlice(newSlice: array<int>) {
        ySlicedBuckets.PushBack(newSlice);
    }
    // ------------------------------------------------------------------------
    protected function prepareDistanceChecks() {
        var i, s, numberOfSlices, ySliceSlot: int;
        var maxY, yPos: Float;
        var newSlice: array<int>;

        numberOfSlices = CeilF(brushRadius / densityDistance);

        s = foliageSet.pos.Size();

        minY = brushCenter.Y - brushRadius - brushParams.maxDistance;
        maxY = brushCenter.Y + brushRadius + brushParams.maxDistance;
        sliceHeight = 1.0 + (maxY - minY) / numberOfSlices;

        for (i = 0; i < numberOfSlices; i += 1) {
            addSlice(newSlice);
        }

        for (i = 0; i < s; i += 1) {
            yPos = foliageSet.pos[i].Y;
            ySliceSlot = FloorF((yPos - minY) / sliceHeight);
            ySlicedBuckets[ySliceSlot].PushBack(i);
        }
    }
    // ------------------------------------------------------------------------
    protected function addToMinDistanceSet(slot: int) {
        var ySliceSlot: int;

        ySliceSlot = FloorF((foliageSet.pos[slot].Y - minY) / sliceHeight);
        ySlicedBuckets[ySliceSlot].PushBack(slot);
    }
    // ------------------------------------------------------------------------
    protected function checkMinimumDistance(
        pos: Vector, type: int, constraints: SRadFoliageMinDistance) : bool
    {
        var i, f, slot: int;
        var foliagePos: Vector;
        var minSlice, maxSlice: int;
        var maxDistanceToTest, delta: float;

        maxDistanceToTest = MaxF(constraints.sameType, constraints.otherType);

        minSlice = Clamp(FloorF((pos.Y - minY - maxDistanceToTest) / sliceHeight), 0, 20000);
        maxSlice = Clamp(FloorF((pos.Y - minY + maxDistanceToTest) / sliceHeight), 0, 20000);

        for (i = minSlice; i <= maxSlice; i += 1) {

            for (f = 0; f < ySlicedBuckets[i].Size(); f += 1) {
                slot = ySlicedBuckets[i][f];
                foliagePos = foliageSet.pos[slot];

                delta = VecLength2D(pos - foliagePos);

                // other type test
                if (delta < constraints.otherType) {
                    return false;
                }

                // same type test
                if (type == foliageSet.type[i] && delta < constraints.sameType) {
                    return false;
                }
            }
        }
        return true;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
