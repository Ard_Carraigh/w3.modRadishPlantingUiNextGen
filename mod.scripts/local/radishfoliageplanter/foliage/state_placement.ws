// ----------------------------------------------------------------------------
state RadFp_Placement in CRadishFoliageEditor extends RadFp_PlantingMode {
    // ------------------------------------------------------------------------
    default currentMode = 'RADFP_PlacementMode';
    default currentModeColor = "blue";
    default maxBrushRadius = 25.0;
    // ------------------------------------------------------------------------
    protected var currentCmd: String;
    protected var lastTrackedPos: SRadishPlacement;
    // ------------------------------------------------------------------------
    protected function formatCurrentParamInfo(optional additional: String) : String
    {
        var info: String;
        // [selected: 1344, moving: 100/1344]

        if (filterByType) {
            if (additional != "") {
                info = "[" + additional + "]";
            }
        } else {
            if (additional != "") {
                info = "[" + GetLocStringByKeyExt("RADFP_lNoActiveFilter")
                    + " " + additional + "]";
            } else {
                info = "[" + GetLocStringByKeyExt("RADFP_lNoActiveFilter") + "]";
            }
        }
        return info;
    }
    // ------------------------------------------------------------------------
    event OnCurrentParamInfo(out info: String) {
        info = formatCurrentParamInfo();
    }
    // ------------------------------------------------------------------------
    // deactivate these commands
    protected function onCmd1Start() {
        currentCmd = "RADFP_Op1";
        super.onCmd1Start();

        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(), brush.getRadius(), getFilteredTypes());

        // since generating foliage for big brush areas can take a while set
        // view status update and...
        parent.view.setOperationInProgress(
            formatCurrentParamInfo(
                GetLocStringByKeyExt("RADFP_lSelectedFoliage")
                + " " + IntToString(foliageSet.pos.Size())
                + " " + GetLocStringByKeyExt("RADFP_lSelectedForMovement")
            ));

        parent.foliageSetBrush.setFoliageSet(foliageSet);
        parent.foliageSetBrush.onPlacementStart();

        lastTrackedPos = parent.tracker.getTrackedPos();
    }
    // ------------------------------------------------------------------------
    protected function onCmd1End() {
        // start replanting to correct ground height positions (async)
        currentCmd = "";

        // update position slots from moved proxies
        parent.foliageSetBrush.onPlacementEnd();

        // this will definitely take a while for bigger brushes
        // -> dedicated progress updates in view
        asyncUpdateTerrainPositions2();
    }
    // ------------------------------------------------------------------------
    private entry function asyncUpdateTerrainPositions2() {
        var counter, currentSlot, i, s, retries, failures: int;
        var p, groundPos: Vector;
        var posOrderedSlots: array<SRadSortKeyValue>;

        s = foliageSet.pos.Size();

        // reorder foliage based on position so the trailing position
        // tracker doesn't need to cross as much ground between subsequent
        // foliage entries in the sequence (-> much faster)
        for (i = 0; i < s; i += 1) {
            p = foliageSet.pos[i];
            posOrderedSlots.PushBack(
                // quantize to 10m stripes
                SRadSortKeyValue(FloorF(p.X / 10.0), RoundF(p.Y), i)
            );
        }
        radMergeSortKeyValue(posOrderedSlots);

        // iterate in position-ordered sequence
        counter = 0;
        while (counter < s) {
            currentSlot = posOrderedSlots[counter].payload;

            if (parent.terrainProbe.getExactGroundPos(foliageSet.pos[currentSlot], groundPos)) {
                retries = 0;
                foliageSet.pos[currentSlot] = groundPos;
                // preceed to next position
                counter += 1;
            } else {
                if (retries >= 10) {
                    LogChannel('DEBUG', "Movement command: failed to get exact terrain height for plant! removing...");
                    foliageSet.type[currentSlot] = -1;
                    failures += 1;
                    counter += 1;
                } else {
                    //LogChannel('DEBUG', "Planting command: updating terrainprobe position to ["
                    //    + VecToString(foliageSet.pos[counter]) + "]");
                    parent.tracker.updateTrackedPos(SRadishPlacement(foliageSet.pos[currentSlot]));
                    retries += 1;

                    parent.view.setOperationInProgress(
                        formatCurrentParamInfo(
                            GetLocStringByKeyExt("RADFP_lSelectedFoliage")
                            + " " + IntToString(foliageSet.pos.Size())
                            + ", " + GetLocStringByKeyExt("RADFP_lFoliageMovement")
                            + " <font color=\"#ff5555\">"
                            + IntToString(counter) + "/" + IntToString(s)
                            + "</font>"
                    ));

                    Sleep(0.1);
                }
            }
        }
        if (failures > 0) {
            parent.error(GetLocStringByKeyExt("RADFP_eFailedFoliageMovementCount") + failures);
        }
        // return to cam position to prevent foliage visibility glitches
        parent.tracker.updateTrackedPos(lastTrackedPos);
        parent.foliageSetBrush.resetFoliageSet();
        parent.editedLayer.updateSet(foliageSet);
        super.onCmd1End();
    }
    // ------------------------------------------------------------------------
    protected function onCmd2Start() {
        currentCmd = "RADFP_Op2";
        super.onCmd2Start();

        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(), brush.getRadius(), getFilteredTypes());

        // since generating foliage for big brush areas can take a while set
        // view status update and...
        parent.view.setOperationInProgress(
            formatCurrentParamInfo(
                GetLocStringByKeyExt("RADFP_lSelectedFoliage")
                + " " + IntToString(foliageSet.pos.Size())
                + " " + GetLocStringByKeyExt("RADFP_lSelectedForRotation")
            ));

        parent.foliageSetBrush.setFoliageSet(foliageSet);
        parent.foliageSetBrush.activateRotationMode(true);
        parent.foliageSetBrush.onPlacementStart();
    }
    // ------------------------------------------------------------------------
    protected function onCmd2End() {
        currentCmd = "";
        // update position slots from moved proxies
        parent.foliageSetBrush.onPlacementEnd();
        parent.foliageSetBrush.activateRotationMode(false);
        parent.foliageSetBrush.resetFoliageSet();
        parent.editedLayer.updateSet(foliageSet);
        super.onCmd2End();
    }
    // ------------------------------------------------------------------------
    protected function onCmd3Start() {}
    // ------------------------------------------------------------------------
    event OnOperation(action: SInputAction) {
        if (!parent.cmdActive && !parent.locked && IsPressed(action)) {
            switch (action.aName) {
                case 'RADFP_Op1': this.onCmd1Start(); break;
                case 'RADFP_Op2': this.onCmd2Start(); break;
                default: return true;
            }
        } else if (IsReleased(action)
            && parent.cmdActive
            //&& !parent.locked ?
            && currentCmd == NameToString(action.aName))
        {
            switch (action.aName) {
                case 'RADFP_Op1': this.onCmd1End(); break;
                case 'RADFP_Op2': this.onCmd2End(); break;
                default: return true;
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op1', "RADFP_MovementOp"));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op2', "RADFP_RotationOp"));
        super.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
