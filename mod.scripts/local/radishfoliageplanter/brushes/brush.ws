// ----------------------------------------------------------------------------
class CRadishFoliageBrush extends IRadishPlaceableElement {
    // ------------------------------------------------------------------------
    protected var stepSize: float; default stepSize = 0.5;
    protected var minSize: float; default minSize = 0.1;
    protected var maxSize: float; default maxSize = 100.0;
    // ------------------------------------------------------------------------
    protected var placement: SRadishPlacement;
    protected var camPos: Vector;
    // ------------------------------------------------------------------------
    protected var size: float; default size = 2.5;
    // ------------------------------------------------------------------------
    protected var proxy: CRadishBrushVisualizer;
    // ------------------------------------------------------------------------
    public function init() {
        proxy = new CRadishBrushVisualizer in this;
        proxy.init(placement, size);
    }
    // ------------------------------------------------------------------------
    public function setSizeConstraint(max: Float) {
        maxSize = max;
        if (size > maxSize) {
            size = maxSize;
            proxy.setRadius(size);
        }
    }
    // ------------------------------------------------------------------------
    public function getCenterPosition() : Vector {
        return this.placement.pos;
    }
    // ------------------------------------------------------------------------
    public function getRadius() : Float {
        return this.size;
    }
    // ------------------------------------------------------------------------
    public function isMinimumSize() : bool {
        return size <= 0.1;
    }
    // ------------------------------------------------------------------------
    public function getPlacement() : SRadishPlacement {
        return placement;
    }
    // ------------------------------------------------------------------------
    public function setCamPosition(pos: Vector) {
        this.camPos = pos;
    }
    // ------------------------------------------------------------------------
    public function setPlacement(newPlacement: SRadishPlacement) {
        placement = newPlacement;
        proxy.setPlacement(newPlacement);
    }
    // ------------------------------------------------------------------------
    public function increaseSize(intensity: int) {
        var distanceModifier: float;

        distanceModifier = ClampF(VecDistance(placement.pos, camPos) / 50.0, 1, 3);

        size = ClampF(size + stepSize * intensity * distanceModifier, minSize, maxSize);
        proxy.setRadius(size);
    }
    // ------------------------------------------------------------------------
    public function decreaseSize(intensity: int) {
        var distanceModifier: float;

        distanceModifier = ClampF(VecDistance(placement.pos, camPos) / 50.0, 1, 3);

        size = ClampF(size - stepSize * intensity * distanceModifier, minSize, maxSize);
        proxy.setRadius(size);
    }
    // ------------------------------------------------------------------------
    public function show(doShow: bool) {
        proxy.show(doShow);
    }
    // ------------------------------------------------------------------------
    public function setColor(color: String) {
        proxy.setColor(color);
    }
    // ------------------------------------------------------------------------
    public function onStartOperation() {
        proxy.setColor("white");
    }
    // ------------------------------------------------------------------------
    public function onEndOperation() {
        proxy.restorePreviousColor();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
