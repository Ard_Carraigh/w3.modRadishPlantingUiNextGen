// ----------------------------------------------------------------------------
abstract class CRadishFoliagePlaceableElement extends IRadishPlaceableElement {
    // ------------------------------------------------------------------------
    protected var isRotationMode: bool;
    // ------------------------------------------------------------------------
    public function isRotationMode() : bool {
        return isRotationMode;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishBrushVisualizer extends CRadishFoliagePlaceableElement {
    // ------------------------------------------------------------------------
    protected var placement: SRadishPlacement;
    protected var radius: float;
    // ------------------------------------------------------------------------
    protected var proxy: CEntity;
    protected var currentColor: String; default currentColor = "green";
    protected var previousColor: String; default previousColor = "green";

    protected var templatePath: String;
    default templatePath = "dlc\modtemplates\radishplanterui\base\brush.w2ent";
    // ------------------------------------------------------------------------
    protected var isVisible: bool;
    // ------------------------------------------------------------------------
    public function init(newPlacement: SRadishPlacement, radius: float) {
        this.placement = newPlacement;
        spawn();
    }
    // ------------------------------------------------------------------------
    protected function spawn() {
        var template: CEntityTemplate;

        if (!proxy) {
            template = (CEntityTemplate)LoadResource(templatePath, true);
            proxy = theGame.CreateEntity(template, placement.pos, placement.rot);
            proxy.AddTag('RADFP_BRUSH');
            setColor("green");

            proxy.SetHideInGame(!isVisible);
        }
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        var null: CEntity;

        proxy.StopAllEffects();
        proxy.Destroy();
        proxy = null;
    }
    // ------------------------------------------------------------------------
    public function setRadius(newRadius: float, optional force: bool) {
        var meshcomp : CComponent;

        if (this.radius != newRadius || force) {
            meshcomp = proxy.GetComponentByClassName('CMeshComponent');
            // height of brush proxy is scaled proportionally to size so it
            // overlays uneven terrain slightly better
            meshcomp.SetScale(Vector(newRadius * 2, newRadius * 2, newRadius * 0.5, 1));

            this.radius = newRadius;
        }
    }
    // ------------------------------------------------------------------------
    // visibility
    // ------------------------------------------------------------------------
    public function toggleVisibility() {
        show(!isVisible);
    }
    // ------------------------------------------------------------------------
    public function setColor(color: String) {
        previousColor = currentColor;
        proxy.ApplyAppearance(color);
        setRadius(this.radius, true);
        currentColor = color;
    }
    // ------------------------------------------------------------------------
    public function restorePreviousColor() {
        setColor(previousColor);
    }
    // ------------------------------------------------------------------------
    public function show(doShow: bool) {
        if (isVisible != doShow) {
            proxy.SetHideInGame(!doShow);
            isVisible = doShow;
        }
    }
    // ------------------------------------------------------------------------
    public function moveTo(placement: SRadishPlacement) {
        this.placement = placement;
        proxy.TeleportWithRotation(placement.pos, placement.rot);
    }
    // ------------------------------------------------------------------------
    public function refreshRepresentation() {
        moveTo(placement);
    }
    // ------------------------------------------------------------------------
    // visualization properties
    // ------------------------------------------------------------------------
    public function getPlacement() : SRadishPlacement {
        return placement;
    }
    // ------------------------------------------------------------------------
    public function setPlacement(newPlacement: SRadishPlacement) {
        placement = newPlacement;
        moveTo(placement);
    }
    // -------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishFoliageSetVisualizer extends CRadishFoliagePlaceableElement {
    // ------------------------------------------------------------------------
    private var previewEnabled: bool; default previewEnabled = false;
    // ------------------------------------------------------------------------
    private var brush: CRadishFoliageBrush;
    private var foliage: CRadishFoliageSet;
    private var terrainProbe: CRadishCachingTerrainProbe;
    private var foliagePreviewer: CRadishFoliagePlacementPreview;
    // ------------------------------------------------------------------------
    public function init(brush: CRadishFoliageBrush, terrainProbe: CRadishCachingTerrainProbe) {
        var template: CEntityTemplate;

        this.brush = brush;
        this.terrainProbe = terrainProbe;

        template = (CEntityTemplate)LoadResource(
            "dlc\modtemplates\radishplanterui\base\radish_placement_preview.w2ent", true);
        foliagePreviewer = (CRadishFoliagePlacementPreview)theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        var null: CRadishFoliagePlacementPreview;

        foliagePreviewer.Destroy();
        foliagePreviewer = null;
    }
    // ------------------------------------------------------------------------
    public function show(doShow: bool) {
        brush.show(doShow);
    }
    // ------------------------------------------------------------------------
    public function activateRotationMode(doActivate: bool) {
        isRotationMode = doActivate;
    }
    // ------------------------------------------------------------------------
    public function setFoliageSet(foliage: CRadishFoliageSet) {
        var comp : CAppearanceComponent;
        var i, s: int;

        this.foliage = foliage;

        s = foliage.proxies.Size();

        for (i = 0; i < s; i += 1) {
            foliage.proxies[i].hideFoliage();
            foliage.proxies[i].showWireframe();
        }
        this.brush.show(false);
    }
    // ------------------------------------------------------------------------
    public function resetFoliageSet() {
        var null: CRadishFoliageSet;
        this.foliage = null;
    }
    // ------------------------------------------------------------------------
    public function getPlacement() : SRadishPlacement {
        return this.brush.getPlacement();
    }
    // ------------------------------------------------------------------------
    public function onPlacementStart() {
        if (previewEnabled && foliage.proxies.Size() < 100) {
            foliagePreviewer.startPreview(foliage);
        }
    }
    // ------------------------------------------------------------------------
    public function onPlacementEnd() {
        var comp : CAppearanceComponent;
        var i, s: int;

        foliagePreviewer.reset();

        s = foliage.pos.Size();
        for (i = 0; i < s; i += 1) {
            foliage.pos[i] = foliage.proxies[i].getWorldPosition();
            foliage.rot[i] = foliage.proxies[i].getWorldRotation();
            foliage.proxies[i].showFoliage();
            foliage.proxies[i].hideWireframe();
        }
        this.brush.show(true);
    }
    // ------------------------------------------------------------------------
    public function setPlacement(newPlacement: SRadishPlacement) {
        var prevPlacement: SRadishPlacement;
        var newPos, delta, groundPos: Vector;
        var i, s: int;
        var newRot, deltaRotation: float;

        prevPlacement = brush.getPlacement();
        delta = prevPlacement.pos - newPlacement.pos;
        deltaRotation = prevPlacement.rot.Yaw - newPlacement.rot.Yaw;

        // calculate the movement delta
        brush.setPlacement(newPlacement);

        foliagePreviewer.onPlacementChanged();

        // use terrain probe to adjust the height for
        s = foliage.proxies.Size();
        //LogChannel('DEBUG', "CRadishFoliageSetVisualizer.setPlacement "
        //    + IntToString(s) + " " + VecToString(delta));
        for (i = 0; i < s; i += 1) {
            newPos = foliage.proxies[i].getWorldPosition() - delta;
            newRot = foliage.proxies[i].getWorldRotation() - deltaRotation;

            if (terrainProbe.getApproximatedGroundPos(newPos, groundPos)) {
                foliage.proxies[i].moveTo(groundPos, newRot);
            } else {
                foliage.proxies[i].moveTo(newPos, newRot);
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
