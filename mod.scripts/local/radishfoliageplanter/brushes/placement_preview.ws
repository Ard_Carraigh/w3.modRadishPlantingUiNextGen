// ----------------------------------------------------------------------------
class CRadishFoliagePlacementPreview extends CEntity {
    // ------------------------------------------------------------------------
    private var foliage: CRadishFoliageSet;
    private var previewShown: bool;
    private var delay: float;
    // ------------------------------------------------------------------------
    public function startPreview(newFoliageSet: CRadishFoliageSet) {
        foliage = newFoliageSet;

        // setup first preview slightly more delayed
        delay = 1.0;
        onPlacementChanged();

        delay = ClampF(foliage.proxies.Size() / 25 , 0.2, 2);
    }
    // ------------------------------------------------------------------------
    public function onPlacementChanged() {
        var i: int;

        if (foliage.proxies.Size() > 0) {

            RemoveTimers();

            if (previewShown) {
                for (i = 0; i < foliage.proxies.Size(); i += 1) {
                    foliage.proxies[i].hideFoliage();
                }
                previewShown = false;
            }

            AddTimer('updateFoliage', delay, false);
        }
    }
    // ------------------------------------------------------------------------
    timer function updateFoliage(delta : float, id : int) {
        var i: int;

        previewShown = true;
        for (i = 0; i < foliage.proxies.Size(); i += 1) {
            foliage.proxies[i].showFoliage();
        }
    }
    // ------------------------------------------------------------------------
    public function reset() {
        var null: CRadishFoliageSet;
        var i: int;

        RemoveTimers();

        for (i = 0; i < foliage.proxies.Size(); i += 1) {
            foliage.proxies[i].hideFoliage();
        }

        foliage = null;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
