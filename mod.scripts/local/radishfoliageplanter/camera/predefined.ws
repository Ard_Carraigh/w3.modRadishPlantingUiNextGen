// ----------------------------------------------------------------------------
enum ERadFpUi_CamSettings {
    // default for initial empty shot
    RadFpUiCam_Empty,
    // ...
}
// ----------------------------------------------------------------------------
function RadFpUi_createCamSettingsFor(type: ERadFpUi_CamSettings) : SRadishPlacement
{
    var s: SRadishPlacement;
    var distance: float;

    switch (type) {
        case RadFpUiCam_Empty:

            if (thePlayer.IsInInterior()) {
                s.pos = theCamera.GetCameraPosition();
                s.rot = theCamera.GetCameraRotation();
            } else {
                s.pos = thePlayer.GetWorldPosition();
                s.rot = thePlayer.GetWorldRotation();
                s.rot.Pitch -= 25;
                s.rot.Roll = 0;
                distance = -5.0;
                s.pos.X -= distance * SinF(Deg2Rad(s.rot.Yaw - 15));
                s.pos.Y += distance * CosF(Deg2Rad(s.rot.Yaw - 15));
                s.pos.Z += 4.0;
                s.pos.W = 1.0;
            }

            break;
        // ...
    }

    return s;
}
// ----------------------------------------------------------------------------
function RadFpUi_getGroundPosFromCam(placement: SRadishPlacement) : Vector {
    var groundZ: Float;
    var pos: Vector;

    pos = placement.pos + MaxF(5, MinF(500, placement.pos.Z)) * VecFromHeading(placement.rot.Yaw);
    pos.Z = placement.pos.Z;
    pos.W = 1.0;

    while (pos.Z > -5.0) {
        if (theGame.GetWorld().PhysicsCorrectZ(pos, groundZ)) {
            pos.Z = groundZ;
            return pos;
        }
        pos.Z -= 1;
    }
    // at this point it seems no ground level data is loaded
    // -> fallback 10 meters in front of cam at camlevel - 3m
    pos = placement.pos + 10 * VecFromHeading(placement.rot.Yaw);
    pos.Z = placement.pos.Z - 3;
    pos.W = 1.0;

    theGame.GetGuiManager().ShowNotification(
        GetLocStringByKeyExt("RADFP_eGroundDetectionFailed"));

    return pos;
}
// ----------------------------------------------------------------------------
